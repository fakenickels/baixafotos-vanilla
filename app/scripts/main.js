var BaixaFotos = {
	init: function(){
		hello.init({
			facebook: 1414700908805329
		});

		+function(){
			var loginBtn = $('.login-btn');

			loginBtn.click(function(){
				hello.login('facebook', {
					scope: 'photos'
				});
			});
		}();		
	},

	app: {
		// https://gist.github.com/HaNdTriX/7704632
		imgToDataURL: function(url, callback, outputFormat, quality) {
			var canvas = document.createElement('CANVAS'),
				ctx = canvas.getContext('2d'),
				img = new Image();
			img.crossOrigin = 'Anonymous';
			img.onload = function() {
				var dataURL;
				canvas.height = img.height;
				canvas.width = img.width;
				try {
					ctx.drawImage(img, 0, 0);
					dataURL = canvas.toDataURL(outputFormat, quality);
					callback(null, dataURL);
				} catch (e) {
					callback(e, null);
				}
				canvas = img = null;
			};
			img.onerror = function() {
				callback(new Error('Could not load image'), null);
			};
			img.src = url;
		},

		// @imagesUrl.albums keep the selected albums and images which user has chose
		imagesUrl: {
			albums: [],
		},

		downloadAndZip: function( callback ){
			var zip = new JSZip(), counter = 0;

			this.imagesUrl.albums.forEach(function(album, i){
				console.log('Dowloading #' + i);
				imgToDataURL( album.source, function(e, data){

					if(!e){
						zip.file( album.id + ".jpg", data.split(',')[1], {base64: true} );
						if( counter === albums.data.length-1 ){
							var content = zip.generate({type:"blob"});
							saveAs(content, "albumsThumbs.zip");

							if( callback ){
								callback();
							}
			   			} else { 
			   				counter++; 
			   			}
			  		} else { 
			  			console.log('hory shito', e) 
			  		}

			  	}, 'image/jpeg');
		 	});
		} 
	}
}